defmodule MoxableUsageExample.Api do
  defmodule MyBehaviour do
    @callback hello() :: atom()
  end

  defmodule Dev do
    @behaviour MyBehaviour
    def hello do
      :dev
    end
  end

  use Moxable, dev_impl: Dev, behaviour: MyBehaviour

  def hello do
    :real
  end
end

defmodule MoxableUsageExample do
  use MoxableUsageExample.Api, as: MyApi

  def hello do
    MyApi.hello()
  end
end
