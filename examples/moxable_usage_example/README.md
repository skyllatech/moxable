# MoxableUsageExample

## Description

This is a basic example of how to use Moxable to simplify setting up and configuring Mox mocks.

Observe that in testing, the call to `MoxableUsageExample.Api` is mocked by Mox.

Run the project in the dev context and the module supplied as the `dev_impl` will be used:

```
# MIX_ENV=dev iex -S mix
> MoxableUsageExample.hello
:dev
> MoxableUsageExample.Api.hello
:real
```

Run the project in the prod context and the real module will be used.
```
# MIX_ENV=prod iex -S mix
> MoxableUsageExample.hello
:real
```
