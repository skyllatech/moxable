defmodule MoxableUsageExample.MixProject do
  use Mix.Project

  def project do
    [
      app: :moxable_usage_example,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:moxable, "~> 0.1.0"},
      {:mox, "~> 1.0", only: :test}
    ]
  end
end
