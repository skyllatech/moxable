defmodule MoxableUsageExampleTest do
  use ExUnit.Case

  import Mox

  use MoxableUsageExample.Api

  test "calls external api to greet the world" do
    expect(Api, :hello, fn -> :mocked end)
    assert MoxableUsageExample.hello() == :mocked
  end
end
