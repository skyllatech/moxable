defmodule Moxable.MixProject do
  use Mix.Project

  @version "0.1.0"
  @git_url "https://gitlab.com/skyllatech/moxable"

  def project do
    [
      app: :moxable,
      version: @version,
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      name: "Moxable",
      description:
        "A simplified method for configuring modules to be mockable with Mox in Elixir projects.",
      deps: deps(),
      docs: docs(),
      package: package()
    ]
  end

  def application, do: []

  defp deps do
    [
      {:mox, "~> 1.0", only: :test},
      {:ex_doc, "~> 0.27", only: :dev, runtime: false}
    ]
  end

  defp docs do
    [
      main: "Moxable",
      source_ref: "v#{@version}",
      source_url: @git_url
    ]
  end

  defp package do
    %{
      licenses: ["MIT"],
      maintainers: ["David Howland"],
      links: %{"Gitlab" => @git_url}
    }
  end
end
