# Moxable

## Description

Moxable is a simplified method for working with Mox in a project.

## Installation

Add Moxable and Mox to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:moxable, "~> 0.1.0"},
    {:mox, "~> 1.0", only: :test}
  ]
end
```

## Usage

See the example project for a simple, complete example and HexDocs for an explanation of usage and options.

## License

Copyright © 2023 Skylla Techologies, Inc.

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the “Software”), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or substantial
  portions of the Software.

  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
  LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
